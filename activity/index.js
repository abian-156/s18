//alert('activity 18');

function Pokemon(name, lvl, hp, atk){
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.attack = atk;

	this.tackle = function (target) {
		console.log(this.name + ' ' + 'tackle' + ' ' + target.name)
	};

	this.hpRem = function (target) {
		this.health = this.health - target.attack;
		console.log(this.name + ' ' + 'remaining health point is' + ' ' + this.health )
	};
	this.faint = function() {

		if (this.health < 10){console.log(this.name + ' ' + 'fainted');
	}

		else {console.log(this.name + ' ' + 'won the battle!')}	
		
	}
}

let bulbasaur = new Pokemon('Bulbasaur', 5, 30, 8);
let pikachu = new Pokemon('pikachu', 10, 20, 13);

pikachu.tackle(bulbasaur);
bulbasaur.hpRem(pikachu);

bulbasaur.tackle(pikachu);
pikachu.hpRem(bulbasaur);

pikachu.tackle(bulbasaur);
bulbasaur.hpRem(pikachu);

bulbasaur.faint()
pikachu.faint()